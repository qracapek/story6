from django.db import models

# Create your models here.
class Matkul(models .Model):
    TAHUN = (
        ('Gasal 2019/2020', 'Gasal 2019/2020'),
        ('Genap 2019/2020', 'Genap 2019/2020'),
        ('Gasal 2020/2021', 'Gasal 2020/2021'),
        ('Genap 2020/2021', 'Genap 2020/2021'),
        ('Gasal 2021/2022', 'Gasal 2021/2022'),
        ('Genap 2021/2022', 'Genap 2021/2022'),
    )
    namamatkul = models.CharField(max_length=200)
    dosen = models.CharField(max_length=200)
    sks = models.IntegerField()
    desc = models.TextField(max_length=100, null=True)
    tahun = models.CharField(max_length=20, choices=TAHUN)
    kelas = models.CharField(max_length=20)

    def __str__(self):
        return self.namamatkul
