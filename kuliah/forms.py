
from django import forms

class createForm(forms.Form):

    TAHUN = (
        ('Gasal 2019/2020', 'Gasal 2019/2020'),
        ('Genap 2019/2020', 'Genap 2019/2020'),
        ('Gasal 2020/2021', 'Gasal 2020/2021'),
        ('Genap 2020/2021', 'Genap 2020/2021'),
        ('Gasal 2021/2022', 'Gasal 2021/2022'),
        ('Genap 2021/2022', 'Genap 2021/2022'),
    )

    namamatkul = forms.CharField(label="Nama Mata Kuliah", widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder':'Nama Mata Kuliah',
        'required':True,
    }))
    dosen = forms.CharField(label="Nama Dosen", widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder':'Nama Dosen',
        'required':True,
        
    }))

    sks = forms.IntegerField(label="Jumlah SKS", widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder':'Jumlah SKS',
        'type':'int',
        'required':True,
    }))

    desc = forms.CharField(label="Deskripsi Mata Kuliah", widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder':'Deskripsi Mata Kuliah',
        'required':True,
    }))
    tahun = forms.CharField(label="Semester Tahun Kuliah", widget=forms.Select(choices=TAHUN))
    kelas = forms.CharField(widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder':'Deskripsi Mata Kuliah',
        'required':True,
    }))