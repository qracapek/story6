## Pipelines Status
[![pipeline status](https://gitlab.com/qracapek/story6/badges/master/pipeline.svg)](https://gitlab.com/qracapek/story6/-/commits/master)

## Code Coverage
[![coverage report](https://gitlab.com/qracapek/story6/badges/master/coverage.svg)](https://gitlab.com/qracapek/story6/-/commits/master)
