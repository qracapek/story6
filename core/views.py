from django.http import HttpResponse
from django.shortcuts import render

def index(request):
    return render(request,"index.html")

def journey(request):
    return render(request, "journey.html")

def recommendations(request):
    return render(request, "recommendations.html")
