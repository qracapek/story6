from django.http import HttpResponse
from django.shortcuts import render, redirect
from .forms import *
from .models import Aktivitas, Individu

# Create your views here.

def addActivities(request):
    create_form = createForm()
    response = {'create_form':create_form}
    return render(request,"activities.html", response)


def add(request):
    form_kegiatan = createForm()
    if request.method == "POST":
        form_kegiatan_input = createForm(request.POST)
        if form_kegiatan_input.is_valid():
            data = form_kegiatan_input.cleaned_data
            kegiatan_input = Aktivitas()
            kegiatan_input.nama = data['nama']
            kegiatan_input.deskripsi = data['deskripsi']
            kegiatan_input.save()
            return redirect('/activities/')
        else:
            return render(request, 'register.html', {'form': form_kegiatan, 'status': 'failed'})
    else:
        return render(request, 'register.html', {'form': form_kegiatan})

def activities(request):
    kegiatan = Aktivitas.objects.all()
    orang = Individu.objects.all()
    return render(request, 'register.html', {'kegiatan': kegiatan, 'orang': orang})

def delete(request, delete_id):
    try:
        jadwal_to_delete = Aktivitas.objects.get(pk = delete_id)
        jadwal_to_delete.delete()
        return redirect('activities')
    except:
        return redirect('activities')

def deleteorang(request, delete_id):
    kegiatan = Aktivitas.objects.all()
    orang = Individu.objects.all()
    print(delete_id)
    orang_to_delete = Individu.objects.get(id=delete_id)
    orang_to_delete.delete()

    return redirect('activities')


def register(request, task_id):
    form_orang = formIndividu()
    if request.method == "POST":
        form_orang_input = formIndividu(request.POST)
        if form_orang_input.is_valid():
            data = form_orang_input.cleaned_data
            orangBaru = Individu()
            orangBaru.namaindividu = data['namaindividu']
            orangBaru.kegiatan = Aktivitas.objects.get(id=task_id)
            orangBaru.save()
            return redirect('/activities/')
        else:
            return render(request, 'nama.html', {'form': form_orang, 'status': 'failed'})
    else:
        return render(request, 'nama.html', {'form': form_orang, 'id':task_id})
