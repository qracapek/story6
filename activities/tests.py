from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from .models import Aktivitas, Individu
from .forms import createForm, formIndividu
from .views import activities, addActivities, add, delete, register, deleteorang
from django.apps import apps
from .apps import ActivitiesConfig

# Create your tests here.
class ModelTest(TestCase):
    def setUp(self):
        self.aktivitas = Aktivitas.objects.create(
            nama="belajar", deskripsi="belajar PPW")
        self.individu = Individu.objects.create(namaindividu="Iqra")

    def test_instance_created(self):
        self.assertEqual(Aktivitas.objects.count(), 1)
        self.assertEqual(Individu.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.aktivitas), "belajar")
        self.assertEqual(str(self.individu), "Iqra")

class FormTest(TestCase):
    def test_form_is_valid(self):
        form_kegiatan = createForm(data={
            "nama": "belajar",
            "deskripsi": "belajar PPW"
        })
        self.assertTrue(form_kegiatan.is_valid())
    
    def test_form_individu_is_valid(self):
        form_orang = formIndividu(data={
            "namaindividu": "Iqra"
        })
        
        self.assertTrue(form_orang.is_valid())

    def test_form_invalid(self):
        form_kegiatan = createForm(data={})
        self.assertFalse(form_kegiatan.is_valid())
        form_orang = formIndividu(data={})
        self.assertFalse(form_orang.is_valid())

class UrlsTest(TestCase):

    def setUp(self):
        self.kegiatan = Aktivitas.objects.create(
            nama="belajar", deskripsi="belajar PPW")
        self.orang = Individu.objects.create(
            namaindividu="Joni", kegiatan=Aktivitas.objects.get(nama="belajar"))
        self.activities = reverse("activities")
        self.add = reverse("add")
        self.addActivities = reverse("addActivities")
        self.register = reverse("register", args=[self.kegiatan.pk])
        self.deleteorang = reverse("deleteorang", args=[self.orang.pk])
        self.delete = reverse("delete", args=[self.kegiatan.pk])

    def test_activities_use_right_function(self):
        found = resolve(self.activities)
        self.assertEqual(found.func, activities)

    def test_add_use_right_function(self):
        found = resolve(self.add)
        self.assertEqual(found.func, add)

    def test_addActivities_use_right_function(self):
        found = resolve(self.addActivities)
        self.assertEqual(found.func, addActivities)

    def test_register_use_right_function(self):
        found = resolve(self.register)
        self.assertEqual(found.func, register)

    def test_deleteorang_use_right_function(self):
        found = resolve(self.deleteorang)
        self.assertEqual(found.func, deleteorang)

    def test_delete_use_right_function(self):
        found = resolve(self.delete)
        self.assertEqual(found.func, delete)

class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.activities = reverse("activities")
        self.add = reverse("add")
        self.addActivities = reverse("addActivities")

    def test_GET_activities(self):
        response = self.client.get(self.activities)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'register.html')

    def test_GET_add(self):
        response = self.client.get(self.add)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'register.html')

    def test_GET_addActivities(self):
        response = self.client.get(self.addActivities)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'activities.html')

    def test_POST_addActivities(self):
        response = self.client.post(self.addActivities,
                                    {
                                        'nama': 'belajar',
                                        'deskripsi': "belajar PPW"
                                    }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_POST_addActivities_invalid(self):
        response = self.client.post(self.addActivities,
                                    {
                                        'nama': '',
                                        'deskripsi': ""
                                    }, follow=True)
        self.assertTemplateUsed(response, 'activities.html')

    def test_GET_deleteorang(self):
        kegiatan = Aktivitas(nama="abc", deskripsi="CDF")
        kegiatan.save()
        orang = Individu(namaindividu="mangoleh",
                      kegiatan=Aktivitas.objects.get(pk=1))
        orang.save()
        response = self.client.get(reverse('deleteorang', args=[orang.pk]))
        self.assertEqual(Individu.objects.count(), 0)
        self.assertEqual(response.status_code, 302)

    def test_GET_delete(self):
        kegiatan = Aktivitas(nama="abc", deskripsi="CDF")
        kegiatan.save()
        
        response = self.client.get(reverse('delete', args=[kegiatan.pk]))
        self.assertEqual(Individu.objects.count(), 0)
        self.assertEqual(response.status_code, 302)

class TestRegist(TestCase):
    def setUp(self):
        kegiatan = Aktivitas(nama="abc", deskripsi="CDF")
        kegiatan.save()

    def test_regist_POST(self):
        response = Client().post('/activities/register/1/',
                                 data={'namaindividu': 'bangjago'})
        self.assertEqual(response.status_code, 302)

    def test_regist_GET(self):
        response = self.client.get('/activities/register/1/')
        self.assertTemplateUsed(response, 'nama.html')
        self.assertEqual(response.status_code, 200)

    def test_regist_POST_invalid(self):
        response = Client().post('/activities/register/1/',
                                 data={'nama': ''})
        self.assertTemplateUsed(response, 'nama.html')
        # self.assertEqual(response.status_code, 302)


class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(ActivitiesConfig.name, 'activities')
        self.assertEqual(apps.get_app_config('activities').name, 'activities')

