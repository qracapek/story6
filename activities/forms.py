from django import forms

class createForm(forms.Form):
    nama = forms.CharField(label="Nama", widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder':'Nama Aktivitas',
        'required':True,
    }))
    deskripsi = forms.CharField(label="Deskripsi", widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder':'Deskripsi',
        'required':True,
        
    }))

class formIndividu(forms.Form):
    namaindividu = forms.CharField(label="Namamu", widget=forms.TextInput(attrs={
        'class':'form-control',
        'placeholder':'Namamu',
        'required':True,
        
    }))