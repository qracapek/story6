from django.db import models

# Create your models here.
class Aktivitas(models .Model):
    nama = models.CharField(max_length=100)
    deskripsi = models.CharField(max_length=1000)
    def __str__(self):
        return self.nama

class Individu(models .Model):
    namaindividu = models.CharField(max_length=64)
    kegiatan = models.ForeignKey(Aktivitas,
                                 on_delete=models.CASCADE, null=True, blank=True)
    def __str__(self):
        return self.namaindividu

