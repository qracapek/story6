from django.contrib import admin
from django.urls import path
from .views import addActivities, add, delete, activities, register, deleteorang

urlpatterns = [
    path('addActivities/', addActivities, name="addActivities"),
    path('add/', add, name='add'),
    path('', activities, name='activities'),
    path('delete/P<int:delete_id>/', delete, name='delete'),
    path('deleteorang/P<int:delete_id>/', deleteorang, name='deleteorang'),
    path('register/<int:task_id>/', register, name='register'),


]
